% PRACTICA PREDICCION COVID-19
% REGRESI�N NO LINEAL
% SERAFIN MARTIN COTANO

%Inicializacion Variables

CCAA=19; % N�mero de Comunidades Aut�nomas
nSim=7; % N�mero de d�as de simulaci�n

% El 15 de Abril es el d�a 55
diaInicial=55;
diaInicialFecha=diaInicial-39;
diaInicialFechaFichero=diaInicialFecha-1;
%Llamamiento a la funci�n de captura de los datos hist�ricos
[output, name_ccaa, iso_ccaa, data_spain] = HistoricDataSpain();


y=length(output.historic{1,1}.DailyCases); %N�mero de d�as con datos almacenados
filay=1:length(output.historic{1,1}.DailyCases); %Vector de datos almacenados
%y=70; %N�mero de d�as con datos almacenados
%filay=1:70; %Vector de datos almacenados
%filay=1:67;

% TOMA DATOS PARA AJUSTAR CURVA DE FORMA GRAFICA
for i= 1:CCAA
variableCasosFitting=output.historic{1}.DailyCases;
variableHospitalizadosFitting=output.historic{1}.Hospitalized;
variableCriticosFitting=output.historic{1}.Critical;
variableMuertosDiariosFitting=output.historic{1}.DailyDeaths;
variableRecuperadosDiariosFitting=output.historic{1}.DailyRecoveries;
%hold on
%plot(variableCasosFitting);
%plot(variableHospitalizadosFitting);
%plot(variableCriticosFitting);
%plot(variableMuertosDiariosFitting);
%plot(variableRecuperadosDiariosFitting);
end

for i = 1:CCAA
    
    % Datos de las variables cargados del fichero de carga
    variableCasos=output.historic{i}.DailyCases;
    variableHospitalizados=output.historic{i}.Hospitalized;
    variableCriticos=output.historic{i}.Critical;
    variableMuertos=output.historic{i}.DailyDeaths;
    variableRecuperados=output.historic{i}.DailyRecoveries;
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % NUMERO DE CASOS 
       foCasos = fitoptions('Method','NonlinearLeastSquares',...
       'StartPoint',[722,39,1.0366,577.8336,42,1.2195,538.0000,34,1.5043,454.7657,44,1.7047,453.4626,37,1.8562,294.0000,52,2.4993],'Lower',[-Inf,-Inf,0,-Inf,-Inf,0,-Inf,-Inf,0,-Inf,-Inf,0,-Inf,-Inf,0,-Inf,-Inf,0],'Upper',[Inf,Inf,Inf,Inf,Inf,Inf,Inf,Inf,Inf,Inf,Inf,Inf,Inf,Inf,Inf,Inf,Inf,Inf]);
       ftCasos = fittype('a1*exp(-((x-b1)/c1)^2) + a2*exp(-((x-b2)/c2)^2) +a3*exp(-((x-b3)/c3)^2) + a4*exp(-((x-b4)/c4)^2)+a5*exp(-((x-b5)/c5)^2) + a6*exp(-((x-b6)/c6)^2)','options',foCasos);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % NUMERO DE HOSPITALIZADOS 
       foHospitalizados =  fitoptions('Method','NonlinearLeastSquares',...
       'StartPoint',[1433,1.6958,0.1044,1.3734e+03,1.4535,0.0885,1.3067e+03,1.2597,0.0801,1.2452e+03,1.0659,0.0721,1.1607e+03,0.9206,0.0728,1.1004e+03,0.7752,0.0722,977.8665,0.6299,0.0808],'Lower',[-Inf,-Inf,0,-Inf,-Inf,0,-Inf,-Inf,0,-Inf,-Inf,0,-Inf,-Inf,0,-Inf,-Inf,0,-Inf,-Inf,0],'Upper',[Inf,Inf,Inf,Inf,Inf,Inf,Inf,Inf,Inf,Inf,Inf,Inf,Inf,Inf,Inf,Inf,Inf,Inf,Inf,Inf,Inf]);
       ftHospitalizados = fittype('a1*exp(-((x-b1)/c1)^2) + a2*exp(-((x-b2)/c2)^2) +a3*exp(-((x-b3)/c3)^2) + a4*exp(-((x-b4)/c4)^2) +a5*exp(-((x-b5)/c5)^2) + a6*exp(-((x-b6)/c6)^2) + a7*exp(-((x-b7)/c7)^2)','options',foHospitalizados);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % NUMERO DE PACIENTES CRITICOS
       foCriticos = fitoptions('Method','NonlinearLeastSquares',...
       'StartPoint',[88,1.6958,0.4277,72.4812,0.7268,0.2884],'Lower',[-Inf,-Inf,0,-Inf,-Inf,0],'Upper',[Inf,Inf,Inf,Inf,Inf,Inf]);
       ftCriticos = fittype('a1*exp(-((x-b1)/c1)^2) + a2*exp(-((x-b2)/c2)^2)','options',foCriticos);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % NUMERO DE MUERTOS  
       foDeaths = fitoptions('Method','NonlinearLeastSquares',...
       'StartPoint',[77,0.3227,0.0510,61.9991,0.4949,0.0655,60.0000,0.0215,0.0806,46.9380,0.6670,0.0962,45.3724,0.1506,0.1183],'Lower',[-Inf,-Inf,0,-Inf,-Inf,0,-Inf,-Inf,0,-Inf,-Inf,0,-Inf,-Inf,0],'Upper',[Inf,Inf,Inf,Inf,Inf,Inf,Inf,Inf,Inf,Inf,Inf,Inf,Inf,Inf,Inf]);
       ftDeaths = fittype('a1*exp(-((x-b1)/c1)^2) + a2*exp(-((x-b2)/c2)^2) + a3*exp(-((x-b3)/c3)^2) + a4*exp(-((x-b4)/c4)^2) + a5*exp(-((x-b5)/c5)^2)','options',foDeaths);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % NUMERO DE RECUPERADOS  
       foRecovery = fitoptions('Method','NonlinearLeastSquares',...
       'StartPoint',[624,1.3125,0.0481,449.0000,1.5277,0.0816,446.0000,1.0973,0.0564,410,0.6670,0.0518,307.3333,1.6568,0.1351,275.6233,0.9682,0.0890],'Lower',[-Inf,-Inf,0,-Inf,-Inf,0,-Inf,-Inf,0,-Inf,-Inf,0,-Inf,-Inf,0,-Inf,-Inf,0],'Upper',[Inf,Inf,Inf,Inf,Inf,Inf,Inf,Inf,Inf,Inf,Inf,Inf,Inf,Inf,Inf,Inf,Inf,Inf]);
       ftRecovery = fittype('a1*exp(-((x-b1)/c1)^2) + a2*exp(-((x-b2)/c2)^2) +a3*exp(-((x-b3)/c3)^2) + a4*exp(-((x-b4)/c4)^2) +a5*exp(-((x-b5)/c5)^2) + a6*exp(-((x-b6)/c6)^2)','options',foRecovery);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
    % Ajuste de la funci�n para cada variable
    fCasos = fit(filay(:),variableCasos(:),ftCasos);
    fHospitalizados = fit(filay(:),variableHospitalizados(:),ftHospitalizados);
    fCriticos = fit(filay(:),variableCriticos(:),ftCriticos);
    fMuertos = fit(filay(:),variableMuertos(:),ftDeaths);
    fRecuperados = fit(filay(:),variableRecuperados(:),ftRecovery);
    
    diaComienzo=diaInicial; %15 de Abril
    % Calculo de la predicci�n para cada variable para 7 d�as
    for j=1:nSim
    YPredCasos(i,j)=fCasos(diaComienzo+j);
    YPredHospitalizados(i,j)=fHospitalizados(diaComienzo+j);
    YPredCriticos(i,j)=fCriticos(diaComienzo+j);
    YPredMuertos(i,j)=fMuertos(diaComienzo+j);
    YPredRecuperados(i,j)=fRecuperados(diaComienzo+j);
    end
    %hold on
    %plot([variableCasos,YPredCasos(i,1:7)]) % dibuja todos los datos, observados y predichos
    %plot([variableHospitalizados,YPredHospitalizados(i,1:7)]) 
    %plot([variableCriticos,YPredCriticos(i,1:7)]) 
    %plot([variableMuertosDiarios,YPredMuertosDiarios(i,1:7)]) 
    %plot([variableRecuperadosDiarios,YPredRecuperadosDiarios(i,1:7)]) 
    
end
% ALMACENAMIENTO DE LOS DATOS EN FICHEROS
fecha=datetime(2020,4,diaInicialFecha);
tabla=[];
tablaErrores=[];
tabla=[tabla;{'ComunidadAutonoma','FECHA','CASOS','Hospitalizados','UCI','Fallecidos','Recuperados'}];
tablaErrores=[tablaErrores;{'ComunidadAutonoma','FECHA','CASOS','Hospitalizados','UCI','Fallecidos','Recuperados'}];
valorDia=diaInicial;

for i=1:nSim
for j=1:CCAA
      
      % FICHEROS PREDICCION
      ComunidadAutonoma=(iso_ccaa{j});
      FECHA=datestr(fecha,'dd/mm/yyyy');
      CASOS=uint16(YPredCasos(j,i));
      Hospitalizados=uint16(YPredHospitalizados(j,i));
      UCI=uint16(YPredCriticos(j,i));
      Fallecidos=uint16(YPredMuertos(j,i));
      Recuperados=uint16(YPredRecuperados(j,i));
      tabla=[tabla;{ComunidadAutonoma,FECHA,CASOS,Hospitalizados,UCI,Fallecidos,Recuperados}];
      
      % FICHEROS PREDICCION ERRORES
      ComunidadAutonoma=(iso_ccaa{j});
      FECHA=datestr(fecha,'dd/mm/yyyy');
      CASOS=(((abs(YPredCasos(j,i)-output.historic{j,1}.DailyCases(valorDia))/YPredCasos(j,i))*100))/16;
      Hospitalizados=(((abs(YPredHospitalizados(j,i)-output.historic{j,1}.Hospitalized(valorDia))/YPredHospitalizados(j,i))*100))/16;
      UCI=(((abs(YPredCriticos(j,i)-output.historic{j,1}.Critical(valorDia))/YPredCriticos(j,i))*100))/16;
      Fallecidos=(((abs(YPredMuertos(j,i)-output.historic{j,1}.DailyDeaths(valorDia))/YPredMuertos(j,i))*100))/16;
      Recuperados=(((abs(YPredRecuperados(j,i)-output.historic{j,1}.DailyRecoveries(valorDia))/YPredRecuperados(j,i))*100))/16;
      tablaErrores=[tablaErrores;{ComunidadAutonoma,FECHA,CASOS,Hospitalizados,UCI,Fallecidos,Recuperados}];
end
%fechaString=datestr(fecha);
%fechaFichero=int2str(diaInicialFechaFichero);
%writecell(tabla,strcat('data/ficherosPrediccion/',fechaFichero,'Abril/','SMC',fechaString,'.csv'));
%writecell(tablaErrores,strcat('data/ficherosPrediccionErrores/',fechaFichero,'AbrilErrores/','SMC',fechaString,'.csv'));
%tabla=[];
%tablaErrores=[];
%tabla=[tabla;{'ComunidadAutonoma','FECHA','CASOS','Hospitalizados','UCI','Fallecidos','Recuperados'}];
%tablaErrores=[tablaErrores;{'ComunidadAutonoma','FECHA','CASOS','Hospitalizados','UCI','Fallecidos','Recuperados'}];
fecha=addtodate(datenum(fecha), 1, 'day');
valorDia=valorDia+1;
end
fechaString=datestr(datetime(2020,4,diaInicialFecha));
fechaFichero=int2str(diaInicialFechaFichero);
writecell(tabla,strcat('data/ficherosPrediccion/',fechaFichero,'Abril/','SMC',fechaString,'.csv'));
writecell(tablaErrores,strcat('data/ficherosPrediccionErrores/',fechaFichero,'AbrilErrores/','SMC',fechaString,'.csv'));






